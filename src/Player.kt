import java.util.*
import java.util.logging.*

val LOG = Logger.getLogger("Player")

class Player (val id: Int, val game: DutchBlitz) {
    val postPiles: List<Stack<Card>>
    val blitzPile = Stack<Card>()
    val woodPile = Stack<Card>()
    val hand = Stack<Card>()

    private val rand = Random()

    init {
        val all_cards = List(40, { i ->
            when (i) {
                in 0 .. 9 -> Card(Color.RED, i + 1, this)
                in 10 .. 19 -> Card(Color.BLUE, i - 9, this)
                in 20 .. 29 -> Card(Color.GREEN, i - 19, this)
                else -> Card(Color.YELLOW, i - 29, this)
            }
        })
        val shuffled = all_cards.shuffled()
        val numPosts = if (game.n_players == 2) 5 else 3
        postPiles = (1 .. numPosts).map { _ -> Stack<Card>() }
        shuffled.slice(0 until numPosts).forEachIndexed { idx, card -> postPiles[idx].push(card) }
        shuffled.slice(numPosts until numPosts + 10).forEach{ c -> blitzPile.push(c) }
        shuffled.slice(numPosts + 10 until 40).forEach { c -> hand.push(c) }
        logAction("postPiles = $postPiles")
        logAction("blitzPile = $blitzPile")
        logAction("hand = $hand")
        logAction("woodPile = $woodPile")
        logAction("---- finished init ----")
    }

    fun start(): Unit {
        while (game.winner == null) {
            Thread.sleep(5000)
            if (blitzPile.isNotEmpty()) {
                performAction()
            } else { // this player wins
                // TODO: turn this into interrupt
                game.winner = this
                logAction("BLITZ!")
            }
        }
    }

    private fun logAction(message: String): Unit {
        LOG.info("Player $id: $message")
    }

    // actions
    private fun performAction(): Unit {
        // when playable
        // first play all 1 cards
        for (p in postPiles) {
            if (tryPlayOneFromPile(p)) {
                val c = blitzPile.peek()
                logAction("move $c from blitz $blitzPile -> $p")
                p.push(blitzPile.pop())
                return
            }
        }

        if (tryPlayOneFromPile(blitzPile)) return
        if (tryPlayOneFromPile(woodPile)) return

        // NOTE: no 1 cards can be played
        for (p in postPiles) {
            if (tryPlayFromPile(p)) {
                val c = blitzPile.peek()
                logAction("move $c from blitz $blitzPile -> $p")
                p.push(blitzPile.pop())
                return
            }
        }
        if (tryPlayFromPile(blitzPile)) return
        if (tryPlayFromPile(woodPile)) return

        // not playable
        flipWood()
        if (tryPlayFromPile(woodPile)) return
        buildPostPiles()
    }

    private fun flipWood(): Unit {
        if (hand.size < 3) {
            while(woodPile.isNotEmpty())
                hand.push(woodPile.pop())
            logAction("put wood pile back into hand")
        }
        woodPile.push(hand.pop())
        woodPile.push(hand.pop())
        woodPile.push(hand.pop())
        logAction("flipped wood, wood: $woodPile")
    }

    private fun buildPostPiles(): Unit {
        // TODO: write build post pile strategy
    }

    private fun tryPlayFromPile(pile: Stack<Card>): Boolean {
        if (pile.isNotEmpty()) {
            val dp = getPlayableDutchPile(pile)
            if (dp != null) {
                val card = pile.peek()
                logAction("play $card (from $pile) -> $dp")
                try {
                    dp.addCard(card)
                    pile.pop()
                } catch (e: InvalidCardException) {
                    LOG.warning("failed to play $card (from $pile) -> $dp: " +
                            "data race occurred")
                }

                return true
            }
        }
        return false
    }

    private fun tryPlayOneFromPile(p: Stack<Card>): Boolean {
        if (p.isNotEmpty()) {
            if (p.peek().number == 1) {
                val dp = DutchPile()
                val c = p.peek()

                logAction("play $c (from $p) -> $dp (newly created)")
                dp.addCard(p.pop())
                game.dutch_piles.add(dp)
                return true
            }
        }
        return false
    }

    private fun getPlayableDutchPile(pile: Stack<Card>): DutchPile? {
        // a pile is playable if the top card:
        // - is a number 1 card -> create a new dutch pile
        // - there is a dutch pile to which the card can be played
        val card = pile.peek()
        assert(card.number != 1)    // all 1 cards should already been played
        var i = 0
        while (true) {
            try {
                val dp = game.dutch_piles.get(i)
                val dp_card = dp.peek()
                if (dp_card?.number == card.number - 1 && dp_card.color == card.color) return dp
                i += 1
            } catch (e: IndexOutOfBoundsException) {
                return null
            }
        }
    }
}