import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import java.util.function.Consumer
import kotlin.collections.ArrayList
import kotlin.concurrent.read
import kotlin.concurrent.write

enum class Color { RED, GREEN, BLUE, YELLOW }
data class Card (val color: Color, val number: Int, val owner: Player) {
    fun isBoy(): Boolean {
        return color == Color.RED || color == Color.BLUE
    }

    override fun toString(): String {
        val clrstr = when (color) {
            Color.RED -> "31"
            Color.GREEN -> "32"
            Color.YELLOW -> "33"
            Color.BLUE -> "34"
        }
        return "\u001B[${clrstr}m" + String.format("%X", number) + "\u001B[0m"
    }
}

class InvalidCardException(val card: Card, val pile: DutchPile): Exception()

class DutchPile {
    private val rwlock = ReentrantReadWriteLock(false)
    private val stack = Stack<Card>()

    fun addCard(card: Card): Unit {
        rwlock.write {
            // first check
            if (stack.size > 0) {
                val top = stack.peek()
                if (top.color != card.color &&
                        top.number != card.number - 1)
                    throw InvalidCardException(card, this)
            }
            stack.push(card)
        }
    }

    fun peek(): Card? { rwlock.read { return stack.peek() } }

    override fun toString(): String {
        return stack.toString()
    }
}

class RWList<T>(initialCapacity: Int) : ArrayList<T>(initialCapacity) {
    private val fairness = false
    private val rwlock = ReentrantReadWriteLock(fairness)
    
    override fun get(index: Int): T { rwlock.read { return super.get(index) } }
    override fun add(element: T): Boolean { rwlock.write { return super.add(element) } }
    override fun iterator(): MutableIterator<T> {
        rwlock.read {
            return super.iterator()
        }
    }
}