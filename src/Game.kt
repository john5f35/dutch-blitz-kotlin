import kotlin.concurrent.thread


class DutchBlitz(val n_players: Int = 4) {
    val players: Array<Player>
    val dutch_piles = RWList<DutchPile>(n_players * 4)
    @Volatile var winner: Player? = null

    init {
        players = Array(n_players, { i -> Player(i, this) })
    }

    fun start(): Unit {
        players.map { p ->
            thread { p.start() }
        }
    }
}

fun main(args: Array<String>) {
    System.setProperty("java.util.logging.SimpleFormatter.format", "%5\$s\n")
    DutchBlitz().start()
}